DROP TABLE IF EXISTS IOT_AGENT_RULE;
DROP TABLE IF EXISTS IOT_AGENT_TOPIC;
DROP TABLE IF EXISTS IOT_AGENT_USER;
DROP TABLE if exists IOT_AGENT;
DROP TABLE if exists USR_USERS_ROLES;
DROP TABLE if exists USR_USERS_LOGING_HISTORY;
DROP TABLE if exists USR_USERS;
DROP TABLE if exists USR_ROLES;
DROP TABLE if exists DIC_DICTIONARY_ITEM;
DROP TABLE if exists DIC_DICTIONARY;


DROP SEQUENCE if exists usr_user_id_sequence;
DROP SEQUENCE if exists USR_ROLES_ID_SEQUENCE;
DROP SEQUENCE if exists USR_USERS_ROLES_ID_SEQUENCE;
DROP SEQUENCE if exists DIC_DICTIONARY_ID_SEQUENCE;
DROP SEQUENCE if exists DIC_DICTIONARY_ITEM_SEQUENCE;
DROP SEQUENCE if exists IOT_AGENT_SEQUENCE;
DROP SEQUENCE if exists IOT_AGENT_USER_SEQUENCE;
DROP SEQUENCE if exists IOT_AGENT_USER_SEQUENCE;
DROP SEQUENCE if exists IOT_AGENT_USER;
DROP SEQUENCE if exists IOT_AGENT_SEQUENCE;
DROP SEQUENCE if exists IOT_AGENT_TOPIC_SEQUENCE;
--
-- USER TABLE
create table USR_USERS
(
    USR_ID             bigint PRIMARY KEY NOT NULL,
    USR_UID            VARCHAR(50)        NOT NULL,
    USR_LOGIN          varchar(50)        NOT NULL,
    USR_PASS           varchar(256)       NOT NULL,
    USR_EMAIL          varchar(256)       NOT NULL,
    USR_ACT            smallint default 1 not null,
    USR_STATUS         smallint DEFAULT 1 not null,
    USR_USER_TYPE_CODE VARCHAR(30)        NOT NULL,
    USR_INSERT_DATE    timestamptz        NOT NULL,
    USR_INSERT_BY      VARCHAR(50)        NOT NULL,
    USR_MODIFY_BY      VARCHAR(50)        NOT NULL,
    USR_MODIFY_DATE    timestamptz        NOT null,
    USR_BLOCK_DATE     timestamptz,
    USR_ERROR_LOGIN    INT      default 0
);
create sequence USR_USER_ID_SEQUENCE
    INCREMENT 1
    START 1;

ALTER TABLE USR_USERS
    ALTER COLUMN USR_ID SET DEFAULT nextval('USR_USER_ID_SEQUENCE');

create table USR_ROLES
(
    RL_ID          bigint PRIMARY KEY NOT NULL,
    RL_CODE        varchar(30)        NOT NULL,
    RL_NAME        varchar(25)        NOT NULL,
    RL_DESCRIPTION varchar(100)       NOT NULL,
    RL_ACT         smallint           NOT NULL
);

create sequence USR_ROLES_ID_SEQUENCE
    INCREMENT 1
    START 1;

ALTER TABLE USR_ROLES
    ALTER COLUMN RL_ID SET DEFAULT nextval('USR_ROLES_ID_SEQUENCE');


create table USR_USERS_ROLES
(
    USR_RL_ID  BIGINT PRIMARY KEY NOT NULL,
    USR_USR_ID bigint             NOT NULL,
    RL_RL_ID   bigint             NOT NULL,
    USR_RL_ACT smallint           NOT NULL,
    FOREIGN KEY (USR_USR_ID) REFERENCES USR_USERS (USR_ID),
    FOREIGN KEY (RL_RL_ID) REFERENCES USR_ROLES (RL_ID)
);

create sequence USR_USERS_ROLES_ID_SEQUENCE
    INCREMENT 1
    START 1;

ALTER TABLE USR_USERS_ROLES
    ALTER COLUMN USR_RL_ID SET DEFAULT nextval('USR_USERS_ROLES_ID_SEQUENCE');

create table USR_USERS_LOGING_HISTORY
(
    USR_LOG_HIS_UID      VARCHAR(50) PRIMARY KEY NOT NULL,
    USR_USR_ID           bigint                  NOT NULL,
    USR_LOG_HIS_ACT      smallint                NOT NULL,
    USR_LOG_HIS_DATA     TIMESTAMP               NOT NULL,
    USR_LOG_HIS_IP       varchar(50),
    USR_LOG_HIS_LOCATION VARCHAR(255),
    USR_LOG_HIS_STATUS   VARCHAR(25)             NOT NULL,
    FOREIGN KEY (USR_USR_ID) REFERENCES USR_USERS (USR_ID)
);

-- SLOWNIKI
create table DIC_DICTIONARY
(
    DIC_DICTIONARY_ID          BIGINT primary key not null,
    DIC_DICTIONARY_UID         varchar(50)        not null,
    DIC_DICTIONARY_CODE        varchar(30)        not null,
    DIC_DICTIONARY_DESCRIPTION varchar(255),
    DIC_DICTIONARY_INSERT_DATE TIMESTAMP          not null,
    DIC_DICTIONARY_MODIFY_DATE TIMESTAMP          not null,
    DIC_DICTIONARY_MODIFY_BY   VARCHAR(50)        not null,
    DIC_DICTIONARY_INSERT_BY   VARCHAR(50)        not null,
    DIC_DICTIONARY_ACT         smallint           not null
);

create sequence DIC_DICTIONARY_ID_SEQUENCE
    INCREMENT 1
    START 1;

ALTER TABLE DIC_DICTIONARY
    ALTER COLUMN DIC_DICTIONARY_ID SET DEFAULT nextval('DIC_DICTIONARY_ID_SEQUENCE');


CREATE TABLE DIC_DICTIONARY_ITEM
(
    DIC_DICTIONARY_ITEM_ID            BIGINT PRIMARY KEY NOT NULL,
    DIC_DICTIONARY_ITEM_UID           varchar(50)        NOT NULL,
    DIC_DICTIONARY_ITEM_CODE          varchar(30)        NOT NULL,
    DIC_DICTIONARY_ITEM_DICTIONARY_ID BIGINT             NOT NULL,
    DIC_DICTIONARY_ITEM_INSERT_DATE   TIMESTAMP          NOT NULL,
    DIC_DICTIONARY_ITEM_JSON          varchar            NOT NULL,
    DIC_DICTIONARY_ITEM_MODIFY_DATE   TIMESTAMP          NOT NULL,
    DIC_DICTIONARY_ITEM_MODIFY_BY     VARCHAR(50)        NOT NULL,
    DIC_DICTIONARY_ITEM_INSERT_BY     VARCHAR(50)        NOT NULL,
    DIC_DICTIONARY_ITEM_ACT           SMALLINT           NOT NULL,
    FOREIGN KEY (DIC_DICTIONARY_ITEM_DICTIONARY_ID) REFERENCES DIC_DICTIONARY (DIC_DICTIONARY_ID)
);

create sequence DIC_DICTIONARY_ITEM_SEQUENCE
    INCREMENT 1
    START 1;

ALTER TABLE DIC_DICTIONARY_ITEM
    ALTER COLUMN DIC_DICTIONARY_ITEM_ID SET DEFAULT nextval('DIC_DICTIONARY_ITEM_SEQUENCE');

--IoT
CREATE TABLE IOT_AGENT
(
    IOT_AGT_ID            BIGINT PRIMARY KEY NOT NULL,
    IOT_AGT_UID           VARCHAR(50)        NOT NULL,
    IOT_AGT_NAME          VARCHAR(50)        NOT NULL,
    IOT_AGT_TYPE_CODE     VARCHAR(50)        NOT NULL,-- kod agenta możliwe. Będą one miały predefiniowane role
    IOT_AGT_CONFIG        varchar,
    IOT_AGT_PUB_KEY_SIGN  varchar,
    IOT_AGT_PUB_KEY_DECR  varchar,
    IOT_AGT_INSERT_BY     VARCHAR(50)        NOT NULL,
    IOT_AGT_MODIFY_BY     VARCHAR(50)        NOT NULL,
    IOT_AGT_INSERT_DATA   TIMESTAMP          NOT NULL,
    IOT_AGT_MODIFY_DATA   TIMESTAMP          NOT NULL,
    IOT_AGT_ACT           SMALLINT           NOT NULL
);

create sequence IOT_AGENT_SEQUENCE
    INCREMENT 1
    START 10;

ALTER TABLE IOT_AGENT
    ALTER COLUMN IOT_AGT_ID SET DEFAULT nextval('IOT_AGENT_SEQUENCE');

CREATE TABLE IOT_AGENT_USER
(
    IOT_AGT_USR_ID BIGINT PRIMARY KEY NOT NULL,
    IOT_AGT_ID     BIGINT             NOT NULL,
    IOT_USR_UID    VARCHAR(50)        NOT NULL,
    FOREIGN KEY (IOT_AGT_ID) REFERENCES IOT_AGENT (IOT_AGT_ID)
);

create sequence IOT_AGENT_USER_SEQUENCE
    INCREMENT 1
    START 10;

ALTER TABLE IOT_AGENT_USER
    ALTER COLUMN IOT_AGT_USR_ID SET DEFAULT nextval('IOT_AGENT_USER_SEQUENCE');

CREATE TABLE IOT_AGENT_TOPIC
(
    IOT_AGR_TOP_ID          BIGINT PRIMARY KEY NOT NULL,
    IOT_AGR_TOP_UID         VARCHAR(50)        NOT NULL,
    IOT_AGT_TOP_ACT         SMALLINT           NOT NULL,
    IOT_AGT_ID              BIGINT             NOT NULL,
    IOT_AGT_TOP_INSERT_BY   VARCHAR(50)        NOT NULL,
    IOT_AGT_TOP_NAME        VARCHAR(255)       NOT NULL,
    IOT_AGT_TOP_MODIFY_BY   VARCHAR(50)        NOT NULL,
    IOT_AGT_TOP_INSERT_DATE TIMESTAMP          NOT NULL,
    IOT_AGT_TOP_MODIFY_DATE TIMESTAMP          NOT NULL,
    FOREIGN KEY (IOT_AGT_ID) REFERENCES IOT_AGENT (IOT_AGT_ID)

);

create sequence IOT_AGENT_TOPIC_SEQUENCE
    INCREMENT 1
    START 10;

ALTER TABLE IOT_AGENT_TOPIC
    ALTER COLUMN IOT_AGR_TOP_ID SET DEFAULT nextval('IOT_AGENT_TOPIC_SEQUENCE');

CREATE TABLE IOT_AGENT_RULE
(
    IOT_AGT_RUL_UID         VARCHAR(50) PRIMARY KEY NOT NULL,
    IOT_AGT_RUL_ACT         SMALLINT                NOT NULL,
    IOT_USR_UID             VARCHAR(50)             NOT NULL,
    IOT_AGT_RUL_CODE        VARCHAR(25)             NOT NULL,
    IOT_AGT_RUL_SETTING     VARCHAR                 NOT NULL,
    IOT_AGT_RUL_INSERT_BY   VARCHAR(50)             NOT NULL,
    IOT_AGT_RUL_MODIFY_BY   VARCHAR(50)             NOT NULL,
    IOT_AGT_RUL_INSERT_DATE TIMESTAMP               NOT NULL,
    IOT_AGT_RUL_MODIFY_DATE TIMESTAMP               NOT NULL
);


-- INIT USERS
INSERT INTO usr_users (usr_login, usr_pass, usr_act, usr_insert_date, usr_modify_date, usr_uid, usr_user_type_code,
                       usr_insert_by, usr_modify_by, USR_EMAIL,USR_STATUS)
VALUES ('ADMIN',
        '98bf3c27b11b01d42cb1acfaf10f6fe44979dab9e3ab9817ac5e376488f9595847bb80299a32cea1603164b82faf525d34cd5521854143e088549dd12ed56559',
        1, '2021-08-31 00:00:00.000000',
        '2021-08-31 00:00:00.000000', '4c09abb9-d383-4b1e-bb49-c8f4937c048b', 'USER', 1, 1,
        'piotr.witowski@student.wat.edu.pl',1);

INSERT INTO usr_users (usr_login, usr_pass, usr_act, usr_insert_date, usr_modify_date, usr_uid, usr_user_type_code,
                       usr_insert_by, usr_modify_by, USR_EMAIL,USR_STATUS)
VALUES ('MGR-USER-ADMINISTRATION',
        '98bf3c27b11b01d42cb1acfaf10f6fe44979dab9e3ab9817ac5e376488f9595847bb80299a32cea1603164b82faf525d34cd5521854143e088549dd12ed56559',
        1, '2020-11-06 19:54:16.665000',
        '2020-11-06 19:54:20.807000', '47f8853b-0b1a-41a8-a71c-e755d71f188a', 'INNER_USER', 1, 1,
        'piotr.witowski@student.wat.edu.pl',1);
INSERT INTO usr_users (usr_login, usr_pass, usr_act, usr_insert_date, usr_modify_date, usr_uid, usr_user_type_code,
                       usr_insert_by, usr_modify_by, USR_EMAIL,USR_STATUS)
VALUES ('MGR-DICT',
        '98bf3c27b11b01d42cb1acfaf10f6fe44979dab9e3ab9817ac5e376488f9595847bb80299a32cea1603164b82faf525d34cd5521854143e088549dd12ed56559',
        1, '2020-11-06 19:54:16.665000',
        '2020-11-06 19:54:20.807000', '47f8853b-0b1a-41a8-a71c-e755d71f188a', 'INNER_USER', 1, 1,
        'piotr.witowski@student.wat.edu.pl',1);
INSERT INTO usr_users (usr_login, usr_pass, usr_act, usr_insert_date, usr_modify_date, usr_uid, usr_user_type_code,
                       usr_insert_by, usr_modify_by, USR_EMAIL,USR_STATUS)
VALUES ('MGR-IOT-ADMINISTRATION',
        '98bf3c27b11b01d42cb1acfaf10f6fe44979dab9e3ab9817ac5e376488f9595847bb80299a32cea1603164b82faf525d34cd5521854143e088549dd12ed56559',
        1, '2020-11-06 19:54:16.665000',
        '2020-11-06 19:54:20.807000', '47f8853b-0b1a-41a8-a71c-e755d71f188a', 'INNER_USER', 1, 1,
        'piotr.witowski@student.wat.edu.pl',1);
INSERT INTO usr_users (usr_login, usr_pass, usr_act, usr_insert_date, usr_modify_date, usr_uid, usr_user_type_code,
                       usr_insert_by, usr_modify_by, USR_EMAIL,USR_STATUS)
VALUES ('MGR-IOT-DATA',
        '98bf3c27b11b01d42cb1acfaf10f6fe44979dab9e3ab9817ac5e376488f9595847bb80299a32cea1603164b82faf525d34cd5521854143e088549dd12ed56559',
        1, '2020-11-06 19:54:16.665000',
        '2020-11-06 19:54:20.807000', '47f8853b-0b1a-41a8-a71c-e755d71f188a', 'INNER_USER', 1, 1,
        'piotr.witowski@student.wat.edu.pl',1);


-- role i uprawnienia
INSERT INTO usr_roles (rl_code, rl_name, rl_act, rl_description)
VALUES ('INNER_USER', 'INNER', 1, 'INNER ROLE');
INSERT INTO usr_roles (rl_code, rl_name, rl_act, rl_description)
VALUES ('ADMIN', 'ADMIN', 1, 'ADMIN ROLE');
INSERT INTO usr_roles (rl_code, rl_name, rl_act, rl_description)
VALUES ('USER', 'USER', 1, 'USER ROLE');


insert into usr_users_roles (rl_rl_id, usr_rl_act, usr_usr_id)
values (2, 1, (select u.USR_ID from USR_USERS u where u.USR_LOGIN = 'ADMIN'));
insert into usr_users_roles (rl_rl_id, usr_rl_act, usr_usr_id)
values (1, 1, (select u.USR_ID from USR_USERS u where u.USR_LOGIN = 'ADMIN'));
insert into usr_users_roles (rl_rl_id, usr_rl_act, usr_usr_id)
values (1, 1, (select u.USR_ID from USR_USERS u where u.USR_LOGIN = 'MGR-USER-ADMINISTRATION'));
insert into usr_users_roles (rl_rl_id, usr_rl_act, usr_usr_id)
values (1, 1, (select u.USR_ID from USR_USERS u where u.USR_LOGIN = 'MGR-DICT'));
insert into usr_users_roles (rl_rl_id, usr_rl_act, usr_usr_id)
values (1, 1, (select u.USR_ID from USR_USERS u where u.USR_LOGIN = 'MGR-IOT-ADMINISTRATION'));
insert into usr_users_roles (rl_rl_id, usr_rl_act, usr_usr_id)
values (1, 1, (select u.USR_ID from USR_USERS u where u.USR_LOGIN = 'MGR-IOT-DATA'));

insert into iot_AGENT(IOT_AGT_ID, IOT_AGT_UID, IOT_AGT_NAME, IOT_AGT_TYPE_CODE, IOT_AGT_CONFIG, IOT_AGT_INSERT_BY,
                      IOT_AGT_MODIFY_BY, IOT_AGT_INSERT_DATA, IOT_AGT_MODIFY_DATA, IOT_AGT_ACT)
VALUES ( 1, 'wdjyz6monw', 'test', 'test', null, 'a8ff7182-d47d-4905-b280-b8813e754f62'
       , 'a8ff7182-d47d-4905-b280-b8813e754f62'
       , '2020-11-06 19:54:20.807000', '2020-11-06 19:54:20.807000', 1);


insert into iot_AGENT(IOT_AGT_ID, IOT_AGT_UID, IOT_AGT_NAME, IOT_AGT_TYPE_CODE, IOT_AGT_CONFIG, IOT_AGT_INSERT_BY,
                      IOT_AGT_MODIFY_BY, IOT_AGT_INSERT_DATA, IOT_AGT_MODIFY_DATA, IOT_AGT_ACT)
VALUES ( 2, 'p8rg8t2tsl', 'test', 'test', null, 'a8ff7182-d47d-4905-b280-b8813e754f62'
       , 'a8ff7182-d47d-4905-b280-b8813e754f62'
       , '2020-11-06 19:54:20.807000', '2020-11-06 19:54:20.807000', 1);


insert into iot_AGENT(IOT_AGT_ID, IOT_AGT_UID, IOT_AGT_NAME, IOT_AGT_TYPE_CODE, IOT_AGT_CONFIG, IOT_AGT_INSERT_BY,
                      IOT_AGT_MODIFY_BY, IOT_AGT_INSERT_DATA, IOT_AGT_MODIFY_DATA, IOT_AGT_ACT)
VALUES ( 3, 'lix8oas2c9', 'test', 'test', null, 'a8ff7182-d47d-4905-b280-b8813e754f62'
       , 'a8ff7182-d47d-4905-b280-b8813e754f62'
       , '2020-11-06 19:54:20.807000', '2020-11-06 19:54:20.807000', 1);


insert into iot_AGENT(IOT_AGT_ID, IOT_AGT_UID, IOT_AGT_NAME, IOT_AGT_TYPE_CODE, IOT_AGT_CONFIG, IOT_AGT_INSERT_BY,
                      IOT_AGT_MODIFY_BY, IOT_AGT_INSERT_DATA, IOT_AGT_MODIFY_DATA, IOT_AGT_ACT)
VALUES ( 4, 'c0snfdmikb', 'test', 'test', null, 'a8ff7182-d47d-4905-b280-b8813e754f62'
       , 'a8ff7182-d47d-4905-b280-b8813e754f62'
       , '2020-11-06 19:54:20.807000', '2020-11-06 19:54:20.807000', 1);


insert into iot_AGENT(IOT_AGT_ID, IOT_AGT_UID, IOT_AGT_NAME, IOT_AGT_TYPE_CODE, IOT_AGT_CONFIG, IOT_AGT_INSERT_BY,
                      IOT_AGT_MODIFY_BY, IOT_AGT_INSERT_DATA, IOT_AGT_MODIFY_DATA, IOT_AGT_ACT)
VALUES ( 5, 'yf7tazczlw', 'test', 'test', null, 'a8ff7182-d47d-4905-b280-b8813e754f62'
       , 'a8ff7182-d47d-4905-b280-b8813e754f62'
       , '2020-11-06 19:54:20.807000', '2020-11-06 19:54:20.807000', 1);


INSERT INTO iot_AGENT_user (iot_AGT_usr_id, iot_AGT_id, iot_usr_uid)
VALUES (1, 1, '4c09abb9-d383-4b1e-bb49-c8f4937c048b');

INSERT INTO iot_AGENT_user (iot_AGT_usr_id, iot_AGT_id, iot_usr_uid)
VALUES (2, 2, '4c09abb9-d383-4b1e-bb49-c8f4937c048b');

INSERT INTO iot_AGENT_user (iot_AGT_usr_id, iot_AGT_id, iot_usr_uid)
VALUES (3, 3, '4c09abb9-d383-4b1e-bb49-c8f4937c048b');

INSERT INTO iot_AGENT_user (iot_AGT_usr_id, iot_AGT_id, iot_usr_uid)
VALUES (4, 4, '4c09abb9-d383-4b1e-bb49-c8f4937c048b');

INSERT INTO iot_AGENT_user (iot_AGT_usr_id, iot_AGT_id, iot_usr_uid)
VALUES (5, 5, '4c09abb9-d383-4b1e-bb49-c8f4937c048b');

--rule
insert into DIC_DICTIONARY(DIC_DICTIONARY_ID, DIC_DICTIONARY_UID, DIC_DICTIONARY_CODE, DIC_DICTIONARY_DESCRIPTION,
                           DIC_DICTIONARY_INSERT_DATE, DIC_DICTIONARY_MODIFY_DATE, DIC_DICTIONARY_MODIFY_BY,
                           DIC_DICTIONARY_INSERT_BY, DIC_DICTIONARY_ACT)
values (1, 'bf25ba81-0094-440f-b1d6-7dc00f2dbb3a'
           , 'RULES', 'agents rules', '2020-11-06 19:54:20.807000', '2020-11-06 19:54:20.807000',
        'a8ff7182-d47d-4905-b280-b8813e754f62', 'a8ff7182-d47d-4905-b280-b8813e754f62', 1);

insert into DIC_DICTIONARY_ITEM(DIC_DICTIONARY_ITEM_ID, DIC_DICTIONARY_ITEM_UID, DIC_DICTIONARY_ITEM_CODE,
                                DIC_DICTIONARY_ITEM_DICTIONARY_ID, DIC_DICTIONARY_ITEM_INSERT_DATE,
                                DIC_DICTIONARY_ITEM_JSON, DIC_DICTIONARY_ITEM_MODIFY_DATE,
                                DIC_DICTIONARY_ITEM_MODIFY_BY, DIC_DICTIONARY_ITEM_INSERT_BY,
                                DIC_DICTIONARY_ITEM_ACT)
values (2, '2c0b802e-0649-4cf2-a219-ad5368a2c928', 'SAVE_RAW_DATA', 1, '2020-11-06 19:54:20.807000', '{}',
        '2020-11-06 19:54:20.807000',
        'a8ff7182-d47d-4905-b280-b8813e754f62', 'a8ff7182-d47d-4905-b280-b8813e754f62', 1);

insert into DIC_DICTIONARY_ITEM(DIC_DICTIONARY_ITEM_ID, DIC_DICTIONARY_ITEM_UID, DIC_DICTIONARY_ITEM_CODE,
                                DIC_DICTIONARY_ITEM_DICTIONARY_ID, DIC_DICTIONARY_ITEM_INSERT_DATE,
                                DIC_DICTIONARY_ITEM_JSON, DIC_DICTIONARY_ITEM_MODIFY_DATE,
                                DIC_DICTIONARY_ITEM_MODIFY_BY, DIC_DICTIONARY_ITEM_INSERT_BY,
                                DIC_DICTIONARY_ITEM_ACT)
values (3, 'bb7bfa21-b25e-4741-827c-080fe9f1e81e', 'SAVE_PROCESSED_DATA', 1, '2020-11-06 19:54:20.807000', '{"isSelect": true,"isFrom": true,"isWhere": true,"isInto": false}',
        '2020-11-06 19:54:20.807000',
        'a8ff7182-d47d-4905-b280-b8813e754f62', 'a8ff7182-d47d-4905-b280-b8813e754f62', 1);

insert into DIC_DICTIONARY_ITEM(DIC_DICTIONARY_ITEM_ID, DIC_DICTIONARY_ITEM_UID, DIC_DICTIONARY_ITEM_CODE,
                                DIC_DICTIONARY_ITEM_DICTIONARY_ID, DIC_DICTIONARY_ITEM_INSERT_DATE,
                                DIC_DICTIONARY_ITEM_JSON, DIC_DICTIONARY_ITEM_MODIFY_DATE,
                                DIC_DICTIONARY_ITEM_MODIFY_BY, DIC_DICTIONARY_ITEM_INSERT_BY,
                                DIC_DICTIONARY_ITEM_ACT)
values (4, '26dfc1aa-2f44-4e7e-b588-c01a232d1876', 'REDIRECT_DATA', 1, '2020-11-06 19:54:20.807000', '{"isSelect": true,"isFrom": true,"isWhere": true,"isInto": true}',
        '2020-11-06 19:54:20.807000',
        'a8ff7182-d47d-4905-b280-b8813e754f62', 'a8ff7182-d47d-4905-b280-b8813e754f62', 1);

--       user types
insert
into DIC_DICTIONARY(DIC_DICTIONARY_ID, DIC_DICTIONARY_UID, DIC_DICTIONARY_CODE, DIC_DICTIONARY_DESCRIPTION,
                    DIC_DICTIONARY_INSERT_DATE, DIC_DICTIONARY_MODIFY_DATE, DIC_DICTIONARY_MODIFY_BY,
                    DIC_DICTIONARY_INSERT_BY, DIC_DICTIONARY_ACT)
values (2, '581b80b7-142b-4033-af7e-cb635b04708b'
           , 'USER_TYPES', 'user types', '2020-11-06 19:54:20.807000', '2020-11-06 19:54:20.807000',
        'a8ff7182-d47d-4905-b280-b8813e754f62', 'a8ff7182-d47d-4905-b280-b8813e754f62', 1);

insert into DIC_DICTIONARY_ITEM(DIC_DICTIONARY_ITEM_ID, DIC_DICTIONARY_ITEM_UID, DIC_DICTIONARY_ITEM_CODE,
                                DIC_DICTIONARY_ITEM_DICTIONARY_ID, DIC_DICTIONARY_ITEM_INSERT_DATE,
                                DIC_DICTIONARY_ITEM_JSON, DIC_DICTIONARY_ITEM_MODIFY_DATE,
                                DIC_DICTIONARY_ITEM_MODIFY_BY, DIC_DICTIONARY_ITEM_INSERT_BY,
                                DIC_DICTIONARY_ITEM_ACT)
values (5, '49b3be63-ce87-4841-beca-2370e11891fa', 'USER', 2, '2020-11-06 19:54:20.807000',
        '{"canUserBeCreated":true,"userRole":"USER"}',
        '2020-11-06 19:54:20.807000',
        'a8ff7182-d47d-4905-b280-b8813e754f62', 'a8ff7182-d47d-4905-b280-b8813e754f62', 1);
insert
into DIC_DICTIONARY_ITEM(DIC_DICTIONARY_ITEM_ID, DIC_DICTIONARY_ITEM_UID, DIC_DICTIONARY_ITEM_CODE,
                         DIC_DICTIONARY_ITEM_DICTIONARY_ID, DIC_DICTIONARY_ITEM_INSERT_DATE,
                         DIC_DICTIONARY_ITEM_JSON, DIC_DICTIONARY_ITEM_MODIFY_DATE,
                         DIC_DICTIONARY_ITEM_MODIFY_BY, DIC_DICTIONARY_ITEM_INSERT_BY,
                         DIC_DICTIONARY_ITEM_ACT)
values (6, '4853f64c-5d32-44ad-a126-9361322e4966', 'INNER_USER', 2, '2020-11-06 19:54:20.807000',
        '{"canUserBeCreated":false,"userRole":"INNER_USER"}',
        '2020-11-06 19:54:20.807000',
        'a8ff7182-d47d-4905-b280-b8813e754f62', 'a8ff7182-d47d-4905-b280-b8813e754f62', 1);

--        AGENT_TYPES


insert
into DIC_DICTIONARY(DIC_DICTIONARY_ID,
                    DIC_DICTIONARY_UID,
                    DIC_DICTIONARY_CODE,
                    DIC_DICTIONARY_DESCRIPTION,
                    DIC_DICTIONARY_INSERT_DATE,
                    DIC_DICTIONARY_MODIFY_DATE,
                    DIC_DICTIONARY_MODIFY_BY,
                    DIC_DICTIONARY_INSERT_BY,
                    DIC_DICTIONARY_ACT)
values (3,
        '581b80b7-142b-4033-af7e-cb635b04708b'
           ,
        'AGENT_TYPES',
        'agent types',
        '2020-11-06 19:54:20.807000',
        '2020-11-06 19:54:20.807000',
        'a8ff7182-d47d-4905-b280-b8813e754f62',
        'a8ff7182-d47d-4905-b280-b8813e754f62',
        1);


insert
into DIC_DICTIONARY_ITEM(DIC_DICTIONARY_ITEM_ID, DIC_DICTIONARY_ITEM_UID, DIC_DICTIONARY_ITEM_CODE,
                         DIC_DICTIONARY_ITEM_DICTIONARY_ID, DIC_DICTIONARY_ITEM_INSERT_DATE,
                         DIC_DICTIONARY_ITEM_JSON, DIC_DICTIONARY_ITEM_MODIFY_DATE,
                         DIC_DICTIONARY_ITEM_MODIFY_BY, DIC_DICTIONARY_ITEM_INSERT_BY,
                         DIC_DICTIONARY_ITEM_ACT)
values (7, '4853f64c-5d32-44ad-a126-9361322e4966', 'BASIC_AGENT', 3, '2020-11-06 19:54:20.807000',
        '{"agentTypeRuleDictList":[{"topic":"test","agentRuleDictList":[{"typeCode":"saveInDb","typeConfig":"select * from "}]}]}',
        '2020-11-06 19:54:20.807000',
        'a8ff7182-d47d-4905-b280-b8813e754f62', 'a8ff7182-d47d-4905-b280-b8813e754f62', 1);


-- rule agent

insert into IOT_AGENT_RULE(IOT_AGT_RUL_UID, IOT_AGT_RUL_ACT, IOT_USR_UID, IOT_AGT_RUL_CODE, IOT_AGT_RUL_SETTING,
                           IOT_AGT_RUL_INSERT_BY, IOT_AGT_RUL_MODIFY_BY, IOT_AGT_RUL_INSERT_DATE,
                           IOT_AGT_RUL_MODIFY_DATE)
values ('26dfc1aa-2f44-4e7e-b588-c01a232d1876', 1, '4c09abb9-d383-4b1e-bb49-c8f4937c048b', 'SAVE_PROCESSED_DATA', '{  "config": "select * from /home/# "}',
        '4c09abb9-d383-4b1e-bb49-c8f4937c048b', '4c09abb9-d383-4b1e-bb49-c8f4937c048b',
        '2020-11-06 19:54:20.807000', '2020-11-06 19:54:20.807000');

insert into IOT_AGENT_RULE(IOT_AGT_RUL_UID, IOT_AGT_RUL_ACT, IOT_USR_UID, IOT_AGT_RUL_CODE, IOT_AGT_RUL_SETTING,
                           IOT_AGT_RUL_INSERT_BY, IOT_AGT_RUL_MODIFY_BY, IOT_AGT_RUL_INSERT_DATE,
                           IOT_AGT_RUL_MODIFY_DATE)
values ('55f94aa8-6594-4c0e-8e54-c32b394fbd77', 1, '4c09abb9-d383-4b1e-bb49-c8f4937c048b', 'REDIRECT_DATA', '{  "config": "select * from /home/garage where temperature > 0 into /home/door/sensor"}',
        '4c09abb9-d383-4b1e-bb49-c8f4937c048b', '4c09abb9-d383-4b1e-bb49-c8f4937c048b',
        '2020-11-06 19:54:20.807000', '2020-11-06 19:54:20.807000');
        
       insert into IOT_AGENT_RULE(IOT_AGT_RUL_UID, IOT_AGT_RUL_ACT, IOT_USR_UID, IOT_AGT_RUL_CODE, IOT_AGT_RUL_SETTING,
                           IOT_AGT_RUL_INSERT_BY, IOT_AGT_RUL_MODIFY_BY, IOT_AGT_RUL_INSERT_DATE,
                           IOT_AGT_RUL_MODIFY_DATE)
values ('8819d3a3-f0c0-4da7-ae66-2c42d7315fb3', 1, '4c09abb9-d383-4b1e-bb49-c8f4937c048b', 'SAVE_RAW_DATA', '{  "config": "select * from /home/# "}',
        '4c09abb9-d383-4b1e-bb49-c8f4937c048b', '4c09abb9-d383-4b1e-bb49-c8f4937c048b',
        '2020-11-06 19:54:20.807000', '2020-11-06 19:54:20.807000');

update iot_agent set iot_agt_pub_key_sign = 'MIICIDANBgkqhkiG9w0BAQEFAAOCAg0AMIICCAKCAf8zOOLVqaDoVKa8BBLYaTS8HeH6e97ZRz4z2GiPNeA31UbZvVFzgz0DEQnQz6IM1lL1mwwBI3QuSZkWb9TMqnO3gAcGNYKCWaSE02/0AjRp54c2Ndc9Np2seZbxTSxllbxzziRwxszAivPPGJfj5P4DUC2y0Q+4y7YKBXBjwiiUHc9f2cgpYKWBX1KaLZ6gjQEDUWGjJ4ypv+HbTbl+v4cbs8VGrxYCtXnWePCtxGb6KnO9nbb/WjTS3+fkaOHsYxPE6VU+DDhPdgU2Ku9NrVXatzVxuRyuEue4aDyrGS0Ct5UM9jwXE53h+vhIvJAOYrDYc3wpFPE47xJlUk5ctj9nf6wc9NfxrtG7tzNBZJpqUtXZqCpHeWZ9D+RyULHyA1F9mKmHSlQf6YD3rCLAHJSsSkTbpjHTwJVYloTq3TmorxCOxFq3FjSj0hZN5W/GC6m7kbhJcGSDVQlzy0AWRDql5ysxHKB2z+pEpvAPIi4B++PjkDD/t0CF1FCS4CfgnHM3i047hdxhQlIqnIR3wp/sF10DOMHK3eu/BoPV66YDxxoNBOF4K4SNd/6zhiX6kFhR8M5LfSCY9Sr1sR4I9XhfCE+K2sWp/3UllBkTauVFuhcq61ob6ebVPzQ2PW5UFwP5PqDH21hbvu/QhOQp59hTzO3j267PN7UIg7J1cPE3AgMBAAE=',
                     iot_agt_pub_key_decr  = 'MIICIDANBgkqhkiG9w0BAQEFAAOCAg0AMIICCAKCAf8qWxRzaeAGxMyaJth7AYG/g4T02TbftZH84QcbEgXHtTLeHIaZDs5yNCX6CauZIK8vFZPfT70A+O1tGh/3OX0/blPta42VG3eeuYXc0A1/6WS9BoxX7Xp9xKRoWv9mFXybJ9K9XXqU+b+7HmgmYji8G6wDSpkzulBokl9hlx961gpRYsWZIXSTcVFznQSgmDHVA6QQJhAKXNAMIzFU4qumI6LkcP+8aMZaDqvjmZU9XGpj1YghDGf/1Fd0e5+JNWzVWI2P/2tgCgxmePc1/sAvJ8EayMJcJbpNfJ/pQHgVzHJredF/cpe4qmKPlOIFC6eoJBSKSd8IQDl9XOutbSWUEPMuBQ8zNLqXsSScn4+7yqmf68OwUayI0vAm/VqsL9MUzeNbAuzjXujLLr4vScTOwCqnpHousu6N7RzEsGSuWhivKXFoluSmxxKpUHLU4Qve2zy7IoGX3kiDwDkS6af39zvZEDhzqNfB07U92Wtkbd1ug9/f+E628q6SYPdm0wzWutgzeQtxVffWYoOmOSLJTJ7eouMwt3IL40tozicO0/93fL/D2uaYo54IZ4b6AKupgZqkP3gmlbzguX/an+PkaOdliMuCkXsXhWuRnXHkY8sHcOlY52EkgjE0ldZhdgyVNDNmo1zLWjwG/4gqX+GvwiFJSOhIeXrueZpWodzRAgMBAAE='
where iot_agt_uid  in ('wdjyz6monw','p8rg8t2tsl','lix8oas2c9','c0snfdmikb','yf7tazczlw');